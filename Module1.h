#ifndef MODULE1_H_INCLUDED
#define MODULE1_H_INCLUDED

#include <iostream>
#include <string>
#include <fstream> //to process files
#include <sstream> //stringstream objects
#include <algorithm>
#include <vector>

using namespace std;

extern string currentKeyword;
extern string currentSymbol;
extern string currentSpecialSymbol;
extern int incurrentKeywordLength;

extern vector <string> KeyWordsVector;
extern vector <string> SymbolsVector;

extern vector <string> MainVector;

//.........Main Functions.................
void processFile( );

void makeTextClearer(fstream& FileWithCode,fstream& temp);

void addToMainVector(fstream& FileWithCode,vector <string>& MainVector);

void distinguishBraces (fstream& FileWithCode,fstream& temp);

void distinguishBlockComments(fstream& FileWithCode,fstream& temp);

void removeBlockComments(fstream& FileWithCode,fstream& temp);

void removeLineComments(fstream& FileWithCode,fstream& temp);

void removeTextInDoubleQuotes(fstream& FileWithCode,fstream& temp);

void distinguishParantheses(fstream& FileWithCode,fstream& temp);

//......................SERVICE FUNCTIONS.................
bool compareToSymbol(vector<string>& obj,string WordFromFile);
bool compareToSpecialSymbol(string SpecialSymbol,string word);
bool compareToSpecialSymbol(vector <string>& obj,string word);
bool compareToKeyWord(vector<string>& obj,string word);
vector<string>::iterator getNextWordFromLine(vector<string>::iterator& CurrentElement);
vector<string>::iterator getPreviousWordFromLine(vector<string>::iterator& CurrentElement);

void swapContentsBack(fstream& FileWithCode,fstream& temp);
void setOriginalText();


#endif // MODULE1_H_INCLUDED
