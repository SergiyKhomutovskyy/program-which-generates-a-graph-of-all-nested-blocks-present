#include "Module1.h"
#include <boost/algorithm/string.hpp>

string currentKeyword;
string currentSymbol;
string currentSpecialSymbol;
int incurrentKeywordLength;
string removeSpaces(string str);

vector <string> KeyWordsVector = {"if","else","while","for","switch","case","do","try","catch","class","struct","enum"};
vector <string> SymbolsVector = {"{","}"};
vector <string> MainVector; //Vector to store key contents

void processFile()
{
    ifstream BaseFile("Base.txt");

    //In order to avoid risk of modifying initial file with code
    //the copy of that file called "Main.txt" is created
    //program is working with Main.txt
    //Temp file is created as well,for the program needs
    ofstream MainFile("Main.txt");
    ofstream TempFile("Temp.txt");

    MainFile << BaseFile.rdbuf();
    MainFile.close();

    fstream FileWithCode;
    fstream temp;

    makeTextClearer(FileWithCode,temp);
    addToMainVector(FileWithCode,MainVector);

}


void makeTextClearer(fstream& FileWithCode,fstream& temp)
{
    //Firstly its important to delete block comments,because
    //they can contain line comments,like:

    /* Examplary Block Comment
    //with
    //line
    //comments*/

    //So,if firstly Line comments will be deleted
    //then the indicator of the current block comment ending -> */
    //will be lost


    distinguishBlockComments(FileWithCode,temp);
    //makes life of removeBlockComments method easier by
    //putting newline before opening and closing block comment identifiers:/*,*/.
    removeBlockComments(FileWithCode,temp);
    //method that removes block comments from the file with source code
    removeLineComments(FileWithCode,temp);
    //method that removes line comments from the file with source code
    removeTextInDoubleQuotes(FileWithCode,temp);
    //method that removes text in between double quotes from the file with source code.
    distinguishBraces(FileWithCode,temp);
    //makes life of addToMainVector method easier by
    //putting newline before opening and closing brackets:{,}.
    distinguishParantheses(FileWithCode,temp);
    //makes life of recognize method easier by
    //putting whitespace before parentheses:(,).

}

void distinguishBlockComments(fstream& FileWithCode,fstream& temp)
{
    FileWithCode.open("Main.txt",ios::in);
    temp.open("Temp.txt",ios::out | ios::trunc );

    if (!FileWithCode || !temp )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {

        string line;

        while (!FileWithCode.eof())
        {
            string lineToWrite;
            std::getline(FileWithCode,line);

            // now process all token in "line"
            std::stringstream in_line(line);
            while (in_line)
            {
                string WordFromLine;  //string to hold the line
                in_line >> WordFromLine;  //getting word from a line to the string

                if(compareToSpecialSymbol("/*",WordFromLine))
                {
                    size_t index = WordFromLine.find("/*");
                    string WordBefore;
                    string WordAfter;
                    for(std::string::size_type i = 0; i < index; ++i)
                    {
                        WordBefore = WordBefore + WordFromLine[i];
                    }

                    for(std::string::size_type i = index + 2 ; i < WordFromLine.size(); ++i)
                    {
                        WordAfter = WordAfter + WordFromLine[i];
                    }

                    if(index > 0)
                    {
                        temp << WordBefore;
                        temp << endl;
                        temp << currentSpecialSymbol;
                        temp <<endl;
                        temp <<WordAfter<<" ";
                    }
                    else
                    {
                        temp << endl;
                        temp << currentSpecialSymbol;
                        temp <<endl;
                        temp <<WordAfter<<" ";
                    }
                }

                else if(compareToSpecialSymbol("*/",WordFromLine))
                {
                    size_t index = WordFromLine.find("*/");
                    string WordBefore;
                    string WordAfter;
                    for(std::string::size_type i = 0; i < index; ++i)
                    {
                        WordBefore = WordBefore + WordFromLine[i];
                    }

                    for(std::string::size_type i = index + 2 ; i < WordFromLine.size(); ++i)
                    {
                        WordAfter = WordAfter + WordFromLine[i];
                    }

                    if(index > 0)
                    {
                        temp << WordBefore;
                        temp << endl;
                        temp << currentSpecialSymbol;
                        temp <<endl;
                        temp <<" "<<WordAfter<<" ";
                    }
                    else
                    {
                        temp << endl;
                        temp << currentSpecialSymbol;
                        temp <<endl;
                        temp <<" "<<WordAfter<<" ";
                    }
                }

                else
                {

                    temp<<" "<<WordFromLine<<" ";
                }
            }
            temp << endl;//putting new line to the file after the line which was processed

        } //while not at the end of file
    } //if file was opened successfully

    FileWithCode.close();
    temp.close();

    swapContentsBack(FileWithCode,temp);

}

void removeBlockComments(fstream& FileWithCode,fstream& temp)
{
    FileWithCode.open("Main.txt",ios::in);
    temp.open("Temp.txt",ios::out | ios::trunc );

    if (!FileWithCode || !temp )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {

        string line;

        while (!FileWithCode.eof())
        {
            string lineToWrite;
            std::getline(FileWithCode,line);

            // now process all token in "line"
            std::stringstream in_line(line);
            while (in_line)
            {
                string WordFromLine;  //string to hold the line
                in_line >> WordFromLine;  //getting word from a line to the string

                if(compareToSpecialSymbol("/*",WordFromLine)) //if block comment beginning indicator was noticed
                {
                    size_t index = WordFromLine.find("/*");//dont really need this

                    while(FileWithCode >> WordFromLine) //until block commend end indicator will be noticed
                    {

                        if(compareToSpecialSymbol("*/",WordFromLine))
                        {
                            break; //continue reading without writing to the file
                        }
                    }
                }

                else
                {

                    temp<<" "<<WordFromLine<<" ";
                }
            }
            temp << endl;//putting new line to the file after the line which was processed

        }//while not at the end of file

    }//if file was opened successfully

    FileWithCode.close();
    temp.close();
    swapContentsBack(FileWithCode,temp);
}


void removeLineComments(fstream& FileWithCode,fstream& temp)
{
    FileWithCode.open("Main.txt",ios::in);
    temp.open("Temp.txt",ios::out | ios::trunc );

    if (!FileWithCode || !temp )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {

        string line;

        while (!FileWithCode.eof())
        {
            string lineToWrite;
            std::getline(FileWithCode,line);

            // now process all token in "line"
            std::stringstream in_line(line);
            while (in_line)
            {
                string WordFromLine;  //string to hold the line
                in_line >> WordFromLine;  //getting word from a line to the string

                if(compareToSpecialSymbol("//",WordFromLine)) //if Line comment beginning indicator was noticed
                {

                    size_t index = WordFromLine.find("//");

                    if(index > 0) //situation when there is no space between word and //:......text//line comment
                    {
                        string WordBefore;

                        for(std::string::size_type i = 0; i < index; ++i)
                        {
                            WordBefore = WordBefore + WordFromLine[i];
                        }

                        temp <<" "<< WordBefore<<" ";
                        break;
                    }

                    break;//break not to write rest of a line from // to temp file
                }

                else
                {
                    temp<<" " << WordFromLine<<" ";
                }
            }//while in line

            temp<<endl;//putting new line to the file after the line which was processed

        }//while not the end of file

    }//if opened successfully

    FileWithCode.close();
    temp.close();

    swapContentsBack(FileWithCode,temp);

}

void removeTextInDoubleQuotes(fstream& FileWithCode,fstream& temp)
{
    FileWithCode.open("Main.txt",ios::in);
    temp.open("Temp.txt",ios::out | ios::trunc);

    if (!FileWithCode || !temp )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {
        char mychar;
        while (!FileWithCode.eof())
        {

            FileWithCode.get(mychar);
            if(mychar == '"')
            {
                //temp << '\n';
                temp << mychar;
                //simply skipping the text to the next occurrence of "
                //deleting everything inside of -->"........."<--
                FileWithCode.get(mychar);
                while(mychar!='"')
                    FileWithCode.get(mychar);

                temp<< mychar;
                // temp << '\n';
            }
            else //if " is not spotted
                temp << mychar;

        }//while not at the end of file
    }//if file was opened successfully
    FileWithCode.close();
    temp.close();
    swapContentsBack(FileWithCode,temp);


}



void distinguishBraces (fstream& FileWithCode,fstream& temp)
{
    FileWithCode.open("Main.txt",ios::in);
    temp.open("Temp.txt",ios::out | ios::trunc);

    if (!FileWithCode || !temp )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {
        char mychar;
        while (!FileWithCode.eof())
        {

            FileWithCode.get(mychar);
            if(mychar == '{' || mychar == '}')
            {
                temp << '\n'; //simply put newline before
                temp << mychar;
                temp << '\n';//simply put newline after
            }
            else
                temp << mychar;
        }
    }
    FileWithCode.close();
    temp.close();
    swapContentsBack(FileWithCode,temp);
}


void distinguishParantheses(fstream& FileWithCode,fstream& temp)
{
    FileWithCode.open("Main.txt",ios::in);
    temp.open("Temp.txt",ios::out | ios::trunc);

    if (!FileWithCode || !temp )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {
        char mychar;
        while (!FileWithCode.eof())
        {

            FileWithCode.get(mychar);

            if(mychar == '(' || mychar == ')')
            {
                temp << ' '; //simply put whitespace before and after (
                temp <<mychar;
                temp << ' ';//simply put whitespace before and after )
            }

            else //if -> ( <- or -> ) <- is not spotted
                temp << mychar;
        }
    }
    FileWithCode.close();
    temp.close();
    swapContentsBack(FileWithCode,temp);

}

void addToMainVector(fstream& FileWithCode,vector <string>& VectorToStoreKeyContents)
{
    FileWithCode.open("Main.txt",ios::in);

    if (!FileWithCode)
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {


        vector<string> Text;
        string WordFromFile;

        while(FileWithCode>>WordFromFile)
        {
            Text.push_back(WordFromFile);
        }

        FileWithCode.close();

        vector<string>::iterator MyIter = Text.begin();
        vector<string>::iterator EndIter = Text.end();

        while(MyIter!=EndIter)
        {
            cout<<*(MyIter)<<endl;
            MyIter++;
        }
        MyIter = Text.begin();

        for(MyIter; MyIter!=EndIter; ++MyIter)
        {

            if(*(MyIter) == "main" && *(getPreviousWordFromLine(MyIter))=="int")
            {
                cout<<"INT MAIN FOUND"<<endl;
                VectorToStoreKeyContents.push_back("int main()");
            }

            if(compareToSpecialSymbol(SymbolsVector,*(MyIter)))
            {
                VectorToStoreKeyContents.push_back(currentSpecialSymbol);
            }

            if(compareToKeyWord(KeyWordsVector,*(MyIter)))
            {
                string Word =  *(MyIter);
                size_t index = Word.find(currentKeyword);
                //string SymbolBefore(1,Word[index - 1]);

                if(index == 0 ) //|| compareToSymbol(SymbolsVector,SymbolBefore)
                {
                    if(Word == "else" && *(getNextWordFromLine(MyIter))=="if")
                    {
                        cout<<"ELSE IF FOUND"<<endl;
                        VectorToStoreKeyContents.push_back("elseif(expression)");
                        ++MyIter;//skiping from else to if
                        ++MyIter;//skipping from if to next element
                        continue;
                    }

                    //if it is a class or structure or enumeration
                    if(currentKeyword == KeyWordsVector[9] || currentKeyword == KeyWordsVector[10]  || currentKeyword == KeyWordsVector[11])
                    {
                        VectorToStoreKeyContents.push_back(currentKeyword + " UserTypeName");
                    }
                    else
                        VectorToStoreKeyContents.push_back(currentKeyword + " (expression)");
                }
            }

        }//while not at the end of Text vector

    }//if file was opened successfully


}

//.......service functions............................

vector<string>::iterator getNextWordFromLine(vector<string>::iterator& CurrentElement) //Pass CONST REFERENCE!
{
    vector<string>::iterator Temp = CurrentElement;
    Temp++;

    return Temp;

}
vector<string>::iterator getPreviousWordFromLine(vector<string>::iterator& CurrentElement) //Pass CONST REFERENCE!
{
    vector<string>::iterator Temp = CurrentElement;
    Temp--;

    return Temp;

}

string removeSpaces(string str)
{
    str.erase(remove(str.begin(), str.end(), ' '), str.end());
    return str;
}

bool compareToSymbol(vector<string>& obj,string WordFromFile)
{
    bool contains = false;
    for(int i = 0; i < obj.size(); i++)
    {
        //size_t index = word.find(obj[i]);
        if(WordFromFile == obj[i])
        {
            contains = true;
            currentSymbol = obj[i];
        }
    }

    return contains;
}

bool compareToSpecialSymbol(string SpecialSymbol,string word)
{
    bool IsEqual = false;
    size_t index = word.find(SpecialSymbol);

    if(index != string::npos)
    {
        IsEqual = true;
        currentSpecialSymbol = SpecialSymbol;
    }

    return IsEqual;
}

bool compareToSpecialSymbol(vector <string>& obj,string word)
{
    bool IsEqual = false;
    for(int i = 0; i < obj.size(); i++)
    {
        size_t index = word.find(obj[i]);

        if(index != string::npos)
        {
            IsEqual = true;
            currentSpecialSymbol = obj[i];
        }

    }

    return IsEqual;
}


//function to be used,when processing a file,and adding main important elements to the main vector
//takes vector of keywords,and checks whether the word from file,contains one of the keywords in the vector
bool compareToKeyWord(vector<string>& obj,string word)
{
    bool contains = false;
    for(int i = 0; i < obj.size(); i++)
    {
        size_t index = word.find(obj[i]);
        if(index != string::npos)
        {
            contains = true;
            currentKeyword = obj[i];
            incurrentKeywordLength = currentKeyword.size();
        }
    }

    return contains;
}



void swapContentsBack(fstream& FileWithCode,fstream& temp)
{
    FileWithCode.open("Main.txt",ios::out | ios :: trunc);
    temp.open("Temp.txt",ios::in);

    if (!FileWithCode || !temp )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {

        string line;

        while (!temp.eof())
        {
            std::getline(temp,line);

            FileWithCode <<line<<endl;
        }
    }

    FileWithCode.close();
    temp.close();
}

void setOriginalText()
{
    fstream original("Base.txt",ios::in);
    fstream FileForProgram("Main.txt",ios::out | ios::trunc);

    if (!original || !FileForProgram )
    {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    else
    {

        string line;

        while (!original.eof())
        {
            std::getline(original,line);
            FileForProgram <<line<<endl;
        }
    }

    FileForProgram.close();
    original.close();
}


