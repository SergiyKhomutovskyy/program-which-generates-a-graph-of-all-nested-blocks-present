#ifndef MODULE2_H_INCLUDED
#define MODULE2_H_INCLUDED

#include <iostream>
#include <string>
//#include <fstream> //to process files
//#include <sstream> //stringstream objects
//#include <algorithm>
#include <vector>



using namespace std;


class GraphNode
{
    private:
        string name;

        vector <GraphNode*> Children; //each Node has its own Children Nodes List

    public:
    GraphNode(string _name) {name = _name;} //constructor

    //CREATE DESTRUCTOR

    void showName(){cout<<name<<endl;}

    string getName(){return this->name;}

    void showChildren()
    {
        int i = 1;
        for (vector <GraphNode*>::iterator pObj = Children.begin();pObj!=Children.end();++pObj)
        {
            cout<<"Children N: "<<i<<" : "<<endl;
            cout<<(*pObj)->name<<endl; //dereferencing iterator gives me pointer on a children Node
            i++;
        }
    }

    void addToChildrenList(GraphNode* someNode)
    {

       Children.push_back(someNode);  //adding Pointer on a Node to the ChildrenList of a given Node

       cout<<"ADDING TO THE CHILDRENLIST DONE!"<<endl;
    }

    bool childrenListEmpty()
    {
        return Children.empty();
    }

    vector<GraphNode*>::iterator getChildrenListBegin()
    {
        vector<GraphNode*>::iterator LocalIter = this->Children.begin();
        return LocalIter;

    }

    vector<GraphNode*>::iterator getChildrenListEnd()
    {
        vector<GraphNode*>::iterator LocalIter = this->Children.end();
        return LocalIter;
    }

    int returnChildrenSize()
    {
        return this->Children.size();
    }
};

//vector to store main Nodes of a program
//such as int main(),or classes,structures etc.
extern vector <GraphNode*> NodesVectorInMain;


//................Main Functions.............
void processMainVector(vector <string>& VectorToProcess);

void recognizeCustomBlocks(vector<string>& MainVector);

void traverseChildren(vector<string>::iterator& CurrentElement,GraphNode* CurrentNode);

//...............service functions.....................

bool isKeyword(string element);

vector<string>::iterator  getNextElement(vector<string>::iterator& CurrentElement); //Pass CONST REFERENCE!

vector<string>::iterator& moveNext(vector<string>::iterator& CurrentElement);//Pass CONST REFERENCE!

void establishConnection(GraphNode* ParentNode);

void displayChildren(GraphNode* CurrentNode,GraphNode* ParentNode);

void specialCheck(vector<string>::iterator& MyIter,vector<string>& MainVector);

#endif // MODULE2_H_INCLUDED
