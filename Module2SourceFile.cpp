#include "Module2.h"

vector <GraphNode*> NodesVectorInMain;

bool isKeyword(string element)
{
    if(element == "{" || element == "}")
        return false;
    else
        return true;

}

vector<string>::iterator  getNextElement(vector<string>::iterator& CurrentElement)
{
    vector<string>::iterator Temp = CurrentElement;
    Temp++;

    return Temp;

}

vector<string>::iterator  getPreviousElement(vector<string>::iterator& CurrentElement)
{
    vector<string>::iterator Temp = CurrentElement;
    --Temp;

    return Temp;

}

vector<string>::iterator& moveNext(vector<string>::iterator& CurrentElement)
{
    CurrentElement++;

    return CurrentElement;
}

vector<string>::iterator& moveNext(vector<string>::iterator& CurrentElement,int n)
{
    for(int i = 0; i<n; ++i)
    {
        CurrentElement++;
    }

    return CurrentElement;
}


void processMainVector(vector <string>& MainVector)
{
    recognizeCustomBlocks(MainVector);

    vector <string>::iterator MyIter =  MainVector.begin();  //iterator to the beginning of the main vector
    vector <string>::iterator EndIter = MainVector.end();   //iterator to the end of the main vector

    while( MyIter!=EndIter )
    {
      if(isKeyword(*MyIter) && *(getNextElement(MyIter)) == "{" )
        {
            GraphNode* Node = new GraphNode(*MyIter); //creating a node with the name of keyword
            NodesVectorInMain.push_back(Node); //pushing this Node to the NodesList
            cout<<"Added to Nodes Vector In Main " <<endl;

            ++MyIter;
            traverseChildren(MyIter,Node);  //traversing the block of code,after the keyword in order to find another nested blocks
        }

        ++MyIter;
    }


}


void recognizeCustomBlocks(vector<string>& MainVector)
{
    vector<string>::iterator MyIter = MainVector.begin();
    vector<string>::iterator EndIter = MainVector.end();
    vector<string> TempVector;
    while(MyIter!=EndIter)
    {
        TempVector.push_back(*MyIter);
        ++MyIter;
    }

    MyIter = TempVector.begin();
    EndIter = TempVector.end();
    MainVector.clear();
    cout<<"TEMP VECTOR CONTENT CB: "<<endl;

    while(MyIter!=EndIter)
        {
           if((*MyIter == "{") && (isKeyword(*(getPreviousElement(MyIter))) == false))
            {
                MainVector.push_back("CustomBlock");

            }
           MainVector.push_back(*MyIter);
           ++MyIter;
        }


        cout<<"MAIN VECTOR WITH CUSTOMBLOCK!"<<endl;

        for(int i = 0;i<MainVector.size();++i)
        {
            cout<<MainVector[i]<<endl;

        }
}

void traverseChildren(vector<string>::iterator& CurrentElement,GraphNode* CurrentNode)
{
    cout<<"IN,CURRENT ELEMENT IS: "<<*CurrentElement<<endl;

    while(*CurrentElement!="}")
    {
        if(isKeyword(*CurrentElement) && *(getNextElement(CurrentElement)) == "{")
        {
            cout<<"................I AM IN THE SUB CONDITION................"<<endl;
            cout<<"CURRENT KEYWORD IS: "<<*CurrentElement<<endl;

            GraphNode* newNode = new GraphNode(*CurrentElement); //create a Node with current keyword as a name

            CurrentNode->addToChildrenList(newNode); //add this Node in children list of a current Node

            ++CurrentElement; //moving to next elem not to repeat keyword condition
            cout<<"......CURRENT ELEMENT:"<<*CurrentElement<<endl;
            cout<<"........MOVING FORWARD......."<<endl;
            traverseChildren(CurrentElement,newNode); //traverse this Node
        }
        else if(*(CurrentElement) == "function_CALL")
        {
            GraphNode* newNode = new GraphNode(*CurrentElement); //create a Node with current keyword as a name

            CurrentNode->addToChildrenList(newNode); //add this Node in children list of a current Node

        }

        ++CurrentElement;
    }


}


//.......................service functions..............................................

void establishConnection(GraphNode* ParentNode)
{
    if(ParentNode->childrenListEmpty())
    {
        cout<<"I AM: "<<ParentNode->getName()<<"...: I HAVE NO CHILDREN"<<endl;
        return;
    }

    vector<GraphNode*>::iterator beg = ParentNode->getChildrenListBegin();
    vector<GraphNode*>::iterator endd = ParentNode->getChildrenListEnd();

    for(beg; beg!=endd; ++beg)
    {
        cout<<"Connection between Parent: "<<ParentNode->getName()<<" AND Child: "<<(*beg)->getName()<<" ESTABLISHED!"<<endl;
        establishConnection((*beg));
    }
}

void displayChildren(GraphNode* CurrentNode,GraphNode* ParentNode)
{
    if(CurrentNode->childrenListEmpty())
    {
        cout<<"I AM: "<<CurrentNode->getName()<<"...:I HAVE NO CHILDREN"<<endl;
        if(CurrentNode!=ParentNode)
        {
            cout<<"MY PARENT IS: "<<ParentNode->getName()<<endl;
        }
        return;
    }
    vector<GraphNode*>::iterator beg = CurrentNode->getChildrenListBegin();
    vector<GraphNode*>::iterator endd = CurrentNode->getChildrenListEnd();

    if(CurrentNode!=ParentNode)
    {
        cout<<"I AM: "<<CurrentNode->getName()<<endl;
        cout<<"MY PARENT IS: "<<ParentNode->getName()<<endl;
    }
    else
    {
        cout<<"I AM: "<<CurrentNode->getName()<<endl;

    }

    int i = 1;
    cout<<"...:MY CHILDREN ARE...:"<<endl;
    for(beg; beg!=endd; ++beg)
    {
        cout<<"CHINDLREN N: "<<i<<" : "<<(*beg)->getName()<<endl;
        ++i;
    }
    beg = CurrentNode->getChildrenListBegin();
    for(beg; beg!=endd; ++beg)
    {
        displayChildren((*beg),CurrentNode);
    }

}

void specialCheck(vector<string>::iterator& MyIter,vector<string>& MainVector)
{
    if(*MyIter == "{" && isKeyword(*(getPreviousElement(MyIter)))==false)
    {
        MyIter = MainVector.insert(MyIter,"CustomBlock");
        ++MyIter;


       while(*MyIter!="}")
        {
            ++MyIter;
            if(*MyIter=="{")
                specialCheck(MyIter,MainVector);
        }
        ++MyIter;
        specialCheck(MyIter,MainVector);
    }
    else
        return;
}

