#ifndef MODULE3_H_INCLUDED
#define MODULE3_H_INCLUDED


#include "Module2.h"  //this module has to know about type GraphNode declared in the Module2

//Including Boost Graph libraries
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/properties.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/named_function_params.hpp>


struct Vertex
{
   string name;
};

//defining a type of adjacency list,called MyGraph
typedef boost::adjacency_list<boost::vecS, boost::vecS,
                                boost::directedS,
                                Vertex,
                                boost::no_property
                                > MyGraph;
//types for vertexes and edges
typedef boost::graph_traits<MyGraph>::vertex_descriptor GraphVertex;
typedef boost::graph_traits<MyGraph>::edge_descriptor GraphEdge;

extern MyGraph Graph;
extern vector<string> NodeNames;

//Create Boost Graph
void createBoostGraph();


//function used by createBoostGraph to traverse all children (and their children) of Nodes in NodesVectorInMain (see Module 2)
//create vertexes and tie them with parent nodes recursively
void processNode(MyGraph& Graph,vector<string>& NodeNames,GraphVertex& ParentVertex,GraphNode* ParentNode);



#endif // MODULE3_H_INCLUDED
