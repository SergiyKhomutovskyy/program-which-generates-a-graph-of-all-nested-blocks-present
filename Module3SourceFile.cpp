#include "Module3.h"

MyGraph Graph;
vector<string> NodeNames;

void createBoostGraph()
{
    //creating root vertex of a graph,called "Program"
    GraphVertex Program = boost::add_vertex(Graph);

    Graph[Program].name = "Program";

    //storing its name to the vector of Vertex names(to be used by write_graphviz function in Module 4)
    NodeNames.push_back("Program");

    //for every Node in NodesVectorInMain
    //store its name to the NodeNames vector (to be used by write_graphviz function in Module 4)
    //create a Boost Graph Vertex for this Node
    //add this vertex to the Boost Graph
    //connect this Vertex and a root Vertex using Boost Graph Edge
    //traverse this Nodes children recursively to establish same connections
    for(int i = 0; i<NodesVectorInMain.size(); ++i)
    {
        NodeNames.push_back(NodesVectorInMain[i]->getName());

        GraphVertex NewlyAddedVertex;
        NewlyAddedVertex = boost::add_vertex(Graph);
        GraphEdge Edge;
        add_edge(Program,NewlyAddedVertex,Graph);

        processNode(Graph,NodeNames,NewlyAddedVertex,NodesVectorInMain[i]);

    }

}

void processNode(MyGraph& Graph,vector<string>& NodeNames,GraphVertex& ParentVertex,GraphNode* ParentNode)
{
    if(ParentNode->childrenListEmpty())
    {
        cout<<"I AM: "<<ParentNode->getName()<<"...: I HAVE NO CHILDREN"<<endl;
        return;
    }

    vector<GraphNode*>::iterator beg = ParentNode->getChildrenListBegin();
    vector<GraphNode*>::iterator endd = ParentNode->getChildrenListEnd();

    //for every children of the ParentNode
    //Create Boost Graph Vertex for this Node
    //Store name of the Node to the NodeNames vector(to be used by write_graphviz function in Module 4)
    //Add this Vertex to the Boost Graph
    //connect this Vertex and a Parent Vertex
    //traverse children of the NewlyAddedVertex recursively,passing this vertex as a last argument
    for(beg; beg!=endd; ++beg)
    {
        GraphVertex NewlyAddedVertex;
        NodeNames.push_back((*beg)->getName());
        NewlyAddedVertex = boost::add_vertex(Graph);
        GraphEdge Edge;
        add_edge(ParentVertex,NewlyAddedVertex,Graph);

        processNode(Graph,NodeNames,NewlyAddedVertex,(*beg));
    }

}

