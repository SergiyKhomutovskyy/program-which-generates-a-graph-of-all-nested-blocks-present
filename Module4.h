#ifndef MODULE4_H_INCLUDED
#define MODULE4_H_INCLUDED

#include "Module3.h"

//function performs visualization of the graph,using
//Boost Graph Library interface to the dot language,used by Graphviz software
void visualizeGraph();

#endif // MODULE4_H_INCLUDED
