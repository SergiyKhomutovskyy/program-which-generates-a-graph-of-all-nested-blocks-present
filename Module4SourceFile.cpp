#include "Module4.h"//this module has to know about Boost Graph initialized in Module 3

void visualizeGraph()
{
    std::ofstream DotFile;
    //open DotFile to write the graph in it
    DotFile.open("D:\\Documents\\education process\\ECOTE\\Project\\EcoteProjectVer1\\Graph.dot");
    //writing a graph to the DotFile
    boost::write_graphviz(DotFile,Graph, boost::make_label_writer(&NodeNames[0]));

    //generate pdf and png files from the dot file
    system("dot -Tpdf Graph.dot -o outputPDF.pdf");
    system("dot -Tpng Graph.dot -o outputPNG.png");

    //system(" dot -Tpng Graph.dot  -Gsize=20\! -Gdpi=900 -Gratio=fill -oexample.png");

    //system("dot -Tpng Graph.dot -o outputPNG.jpeg");
    //system("dot -Tpng Graph.dot -o outputPNG.bmp");

    //open the generated pdf
    system("outputPNG.png");
}
