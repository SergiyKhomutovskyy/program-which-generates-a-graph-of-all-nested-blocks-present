#include <iostream>
#include <string>
#include <fstream> //to process files
#include <sstream> //stringstream objects
#include <algorithm>
#include <vector>
#include <set>

using namespace std;

class A
{
private:

class B//Class B starts here
{

private:
/*Some private variables in class B*/
int k,l;
float m;

class C
{
private:
/*Some
private variables
in class C*/
int x;//Line Comment
int y;//Line Comment


struct inC
{
 string str;
 void set(string s);

 class Y
{
private:
enum Cars { Toyota,Audi,BMW,Bentley};
public:
void Ymethod1();
void Ymethod2();
void Ymethod3();
}; //class Y finish

};//structure inC finish


public:
void get();
void set();
void Cdosmth1(int c,int j,char y);
void Cdosmth2(string a,int b);

};//class C finish

public://Class B public variables and methods:
void Bdosmth1(int c,int j,char y);
void Bdosmth2(string a,int b);

};//class B finish

class D
{

private:
/*Some private variables in class D*/
int k,l;
float m;

class E
{
private:
/*Some
private variables
in class E*/
int x;//Line Comment
int y;//Line Comment

struct inE
{
 string str;
 void set(string s);

  class Z
{
private:
enum Systems { MAC,Windws,Android,Linux,Unix};
public:
void Ymethod1();
void Ymethod2();
void Ymethod3();
}; //class Z finish
}; //struct inE finish



public:
void get();
void set();
void Cdosmth1(int c,int j,char y);
void Cdosmth2(string a,int b);

};//class E finish

public://Class B public variables and methods:
void Bdosmth1(int c,int j,char y);
void Bdosmth2(string a,int b);

};//class D finish


public:
/*Class A public
variables and
methods*/string str;
//Line Comment
char* c;
bool flag;
void Adosmth1(int c,int j,char y);
void Adosmth2(string a,int b);


};//Class A finish



enum Color {red, green, blue,yellow,black};

enum season { spring, summer, autumn, winter };

enum DayOfTheWeek{Mon, Tue, Wed, Thur, Fri, Sat, Sun};



int main()
{
//variables in main():
int x = 100,y = 30,z = 10;
string s;

{//custom block A
  int resultSum = x + y + z;
  int resultSquare;
  resultSquare = resultSum*resultSum;

  {//custom block B

   int resultSubstr = x - y - z;
   int resultCube;
   resultCube = resultSubstr*resultSubstr*resultSubstr;
	{//custom block C
 	 int resultMultplctn = x*y*z;
  	 float resultDivBy2;
   	 resultDivBy2 = resultMultplctn/2;

   	 cout<<"[in custom block C]:resultMultplctn is: "<<resultMultplctn<<endl;
     cout<<"[in custom block C]:resultDivBy2 is: "<<resultDivBy2<<endl;

   	 cout<<"[in custom block C]:ResultSum is: "<<resultSum<<endl;
     cout<<"[in custom block C]:ResultSquare is: "<<resultSquare<<endl;

     cout<<"[in custom block C]:ResultSubstr is: "<<resultSubstr<<endl;
     cout<<"[in custom block C]:ResultCube is: "<<resultCube<<endl;
   	 /*variables belonging to custom block B and A are not seen in this scope*/

   	 resultDivBy2 = resultDivBy2+1000;
   	 if(resultDivBy2>3000)
     {
         int j = 0;
         while(j<5)
         {
             cout<<"More than 3000"<<endl;
             j++;
         }
     }
	}


  cout<<"[in custom block B]:ResultSum is: "<<resultSum<<endl;
  cout<<"[in custom block B]:ResultSquare is: "<<resultSquare<<endl;

  cout<<"[in custom block B]:ResultSubstr is: "<<resultSubstr<<endl;
  cout<<"[in custom block B]:ResultCube is: "<<resultCube<<endl;

  /*variables belonging to custom block C are not seen in this scope*/

  }

  cout<<"[in custom block A]:ResultSum is: "<<resultSum<<endl;
  cout<<"[in custom block A]:ResultSquare is: "<<resultSquare<<endl;
  /*variables belonging to custom block B and C are not seen in this scope*/

}


//...........................Repeating the same as above 2.........................................................
{//custom block A
  int resultSum = x + y + z;
  int resultSquare;
  resultSquare = resultSum*resultSum;

  {//custom block B

   int resultSubstr = x - y - z;
   int resultCube;
   resultCube = resultSubstr*resultSubstr*resultSubstr;
	{//custom block C
 	 int resultMultplctn = x*y*z;
  	 float resultDivBy2;
   	 resultDivBy2 = resultMultplctn/2;

   	 cout<<"[in custom block C]:resultMultplctn is: "<<resultMultplctn<<endl;
     cout<<"[in custom block C]:resultDivBy2 is: "<<resultDivBy2<<endl;

   	 cout<<"[in custom block C]:ResultSum is: "<<resultSum<<endl;
     cout<<"[in custom block C]:ResultSquare is: "<<resultSquare<<endl;

     cout<<"[in custom block C]:ResultSubstr is: "<<resultSubstr<<endl;
     cout<<"[in custom block C]:ResultCube is: "<<resultCube<<endl;
   	 /*variables belonging to custom block B and A are not seen in this scope*/

   	  	 if(resultDivBy2>3000)
        {
         int j = 0;
         do
         {
             cout<<"More than 3000"<<endl;
             j++;
         }while(j<5);
        }

	}


  cout<<"[in custom block B]:ResultSum is: "<<resultSum<<endl;
  cout<<"[in custom block B]:ResultSquare is: "<<resultSquare<<endl;

  cout<<"[in custom block B]:ResultSubstr is: "<<resultSubstr<<endl;
  cout<<"[in custom block B]:ResultCube is: "<<resultCube<<endl;

  /*variables belonging to custom block C are not seen in this scope*/

  }

  cout<<"[in custom block A]:ResultSum is: "<<resultSum<<endl;
  cout<<"[in custom block A]:ResultSquare is: "<<resultSquare<<endl;
  /*variables belonging to custom block B and C are not seen in this scope*/

}

//...........................Repeating the same as above 2.........................................................

{//custom block A
  int resultSum = x + y + z;
  int resultSquare;
  resultSquare = resultSum*resultSum;

  {//custom block B

   int resultSubstr = x - y - z;
   int resultCube;
   resultCube = resultSubstr*resultSubstr*resultSubstr;
	{//custom block C
 	 int resultMultplctn = x*y*z;
  	 float resultDivBy2;
   	 resultDivBy2 = resultMultplctn/2;

   	 cout<<"[in custom block C]:resultMultplctn is: "<<resultMultplctn<<endl;
     cout<<"[in custom block C]:resultDivBy2 is: "<<resultDivBy2<<endl;

   	 cout<<"[in custom block C]:ResultSum is: "<<resultSum<<endl;
     cout<<"[in custom block C]:ResultSquare is: "<<resultSquare<<endl;

     cout<<"[in custom block C]:ResultSubstr is: "<<resultSubstr<<endl;
     cout<<"[in custom block C]:ResultCube is: "<<resultCube<<endl;
   	 /*variables belonging to custom block B and A are not seen in this scope*/

   	  if(resultDivBy2>3000)
        {
         for(int i = 0;i<5;i++)
         {
             cout<<"More than 3000"<<endl;
         }
        }
	}


  cout<<"[in custom block B]:ResultSum is: "<<resultSum<<endl;
  cout<<"[in custom block B]:ResultSquare is: "<<resultSquare<<endl;

  cout<<"[in custom block B]:ResultSubstr is: "<<resultSubstr<<endl;
  cout<<"[in custom block B]:ResultCube is: "<<resultCube<<endl;

  /*variables belonging to custom block C are not seen in this scope*/

  }

  cout<<"[in custom block A]:ResultSum is: "<<resultSum<<endl;
  cout<<"[in custom block A]:ResultSquare is: "<<resultSquare<<endl;
  /*variables belonging to custom block B and C are not seen in this scope*/

}


//taking number from input
int NumberFromUser;
cout<<"PLEASE PROVIDE A NUMBER: "<<endl;
cin>>NumberFromUser;

if(NumberFromUser>1000)//if x is bigger than 100
{/*Code related to the part
    if number from user
      is greater than 1000 */
   int y = 5;
  //While loop start
   while(NumberFromUser < 1000000)
   {
       if(NumberFromUser%2 == 0)
       {
           cout<<"Number is even..."<<endl;
           for(int i = 0;i<5;i++)
           {
               cout<<"Counting i 5 times"<<endl;
               cout<<"i is: "<<i<<endl;
               int j = 0;
               do
               {
                   cout<<"Also j is: "<<j<<endl;
                   j++;

               }while(j!=3);

            //ONE line instructions:
           //This will NOT be added to the graph because of
           //absence of { and }

           if(NumberFromUser == 1003)
                cout<<"Equals to 1003"<<endl;
           else if(NumberFromUser == 1005)
                cout<<"Equals to 1005"<<endl;
            else if(NumberFromUser == 1007)
                cout<<"Equals to 1007"<<endl;
            else
                cout<<"Equals to another odd number"<<endl;
           }
       }
       else
       {

          cout<<"Not an even number..."<<endl;

           {
               int x = 100;
               int powerTwo = x*x;
               {
                   int powerThree = x*x*x;
               }
           }
       }

      NumberFromUser = NumberFromUser + y;
   }
}
else
{
    cout<<"Please provide any number between 1 and 4"<<endl;
    int OneMoreNumber;
    cin>>OneMoreNumber;

    switch(OneMoreNumber)
    {
    case 1:
        {
        for(int cntr = 0;cntr < 2;cntr++)
        {
            cout<<"Number from user is 1"<<endl;
                {
                 int x = 25;
                 x = x*x;
                 cout<<x<<endl;
                }
        }
        break;
        }
    case 2:
        {
        int j = 0;;
        while(j!=2)
        {
            cout<<"Number from user is 2 "<<endl;
            j++;
                {
                 int y = 35;
                 y = y*y;
                 cout<<y<<endl;
                }
        }
        break;
        }
    case 3:
        {
        int m = 2;
        do
        {
          cout<<"Number from user is 3 "<<endl;
          m--;
                {
                 int z = 45;
                 z = z*z;
                 cout<<z<<endl;
                }

        }while(m>0);
        break;
        }
    case 4:
        {
        if((NumberFromUser*OneMoreNumber)%2==0)
        {
            cout<<"EVEN "<<endl;
                {
                 int k = 50;
                 k = k*k;
                 cout<<k<<endl;
                }
        }
        else
        {
            cout<<"ODD "<<endl;
               {
                 int l = 55;
                 l =l*l;
                 cout<<l<<endl;
                }
        }
        break;
        }

    }

}

try
{
    try
    {
        throw std::runtime_error("Test");
    }
    catch (std::runtime_error& e)
    {
        std::cerr << "Inner Exception-Handler: " << e.what() << std::endl;
        throw;
    }
}
catch (std::exception& e)
{
    std::cerr << "Outer Exception-Handler: " << e.what() << std::endl;
}

 try{
    try
    {
      throw 20;
    }
    catch (int e)
    {
      cout << "An exception occured. Exception Number: " << e << endl;
      throw 'e';
    }
  }
  catch(char c)
  {
    cout << "An exception occured. Exception type: " << c ;
  }

return 0;

}//end of main function
