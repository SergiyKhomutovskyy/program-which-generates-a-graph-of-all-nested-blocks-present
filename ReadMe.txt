1)Boost Graph Library has to be installed and configured.

2)Graphviz visualization Software has to be installed and configured.

3)The source code,which is to be processed by the program has to be located
in the project folder (Of course,this file can be located in any folder
,but for that,little modifications in the Module 1 are required).

4)To avoid modifying the original file,program creates files:Main.txt and Temp.txt
for its needs.They can be created before,else program creates them by itself.

5)Program's output are pdf and png versions of the Graph of Nested Blocks,they will be located in the programs folder.