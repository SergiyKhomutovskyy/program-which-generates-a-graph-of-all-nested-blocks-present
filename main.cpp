//#include <iostream>
#include "Module1.h"
#include "Module2.h"
#include "Module3.h"
#include "Module4.h"

int main()
{
    //......Module1......
    processFile( ); //processing input file with source code and storing important content to the MainVector -> Module1

    cout<<"MAIN VECTOR BEFORE MODULE 2: "<<endl;
    for(int i = 0;i<MainVector.size();++i)
    {
        cout<<MainVector[i]<<endl;
    }


    //......Module2......
    processMainVector(MainVector); //processing MainVector (declared in Module 1) and decide on parent-child relationships between Nodes -> Module 2




    vector<GraphNode*>::iterator MyIter = NodesVectorInMain.begin();
    vector<GraphNode*>::iterator EndIter = NodesVectorInMain.end();

    cout<<"NODESVECTORINMAIN contents: "<<endl;
    for(int i = 0; i<NodesVectorInMain.size(); ++i)
    {
        cout<<"Current Node is: "<<NodesVectorInMain[i]->getName()<<endl;
        cout<<"This Node has that many Children: "<<NodesVectorInMain[i]->returnChildrenSize()<<endl;
        establishConnection(NodesVectorInMain[i]);
    }

    cout<<"Nodes Children: "<<endl;
    for(int i = 0; i < NodesVectorInMain.size(); i++)
    {
        cout<<endl;
        cout<<"...................................................................."<<endl;
        cout<<"CHILDREN OF NODE: "<<NodesVectorInMain[i]->getName()<<" FROM MAIN NODES LIST"<<endl;
        displayChildren(NodesVectorInMain[i],NodesVectorInMain[i]);
        cout<<"...................................................................."<<endl;
        cout<<endl;
    }

    //......Module3......
    createBoostGraph(); //Module 3 - > Creating a Boost Graph,based on parent-child relationships derived in Module 2

    //......Module4......
    visualizeGraph();  //Module 4 -> Visualizing a Boost Graph using BGL interface to dot language


    setOriginalText();  //Returns the file with code to its original state(before applying transformation rules defined in Module 1)


    return 0;
}
